package com.example.tasklecture6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.min

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        /*plusButton.setOnClickListener(this)
        minusButton.setOnClickListener(this)*/


    }

    private fun init() {
        minusButton.setOnLongClickListener {
            minus()
            true
        }

        plusButton.setOnClickListener {
            plus()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            minusButton -> minus()
            plusButton -> plus()

        }

    }

    fun plus() {
        var number = findViewById<TextView>(R.id.number).text.toString().toInt()
        if (number < 10) {
            number++
            val tex = findViewById<TextView>(R.id.number)
            tex.text = number.toString()
        }
    }

    fun minus() {
        var number = findViewById<TextView>(R.id.number).text.toString().toInt()
        if (number > 0) {
            number--
            val tex = findViewById<TextView>(R.id.number)
            tex.text = number.toString()
        }

    }

}
